#include "Bishop.h"

Bishop::Bishop(Point place, int color) : Piece(place, color, BISHOP)
{}
Bishop::~Bishop() {}
/*
this function checks for the validation of the Piece move
input dst destination, b Board of game
output: bool is it a valid Move
*/
bool Bishop::checkMove(Point dst, Board b)
{
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    else if (abs(dst.getY() - _place.getY()) == abs(dst.getX() - _place.getX()))
    {
        Point src(_place.getX(), _place.getY());//tmp holder of piece spot
        //checking direction
        if (dst.getY() > _place.getY())
        {
            if (dst.getX() > _place.getX())
            {

                //checking path is clear
                for (int i = _place.getY() + 1; i < dst.getY(); i++)
                {
                    src.setX(src.getX() + 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            else if (dst.getX() < _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() + 1; i < dst.getY(); i++)
                {
                    src.setX(src.getX() - 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;

                }
            }
        }

        else if (dst.getY() < _place.getY())
        {
            if (dst.getX() > _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() - 1; i > dst.getY(); i--)
                {
                    src.setX(src.getX() + 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            else if (dst.getX() < _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() - 1; i > dst.getY(); i--)
                {
                    src.setX(src.getX() - 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
        }
        return true;
    }
    return false;
}    
