#include "Knight.h"


Knight::Knight() {}
Knight::Knight(Point place, int color) : Piece(place, color, KNIGHT) {}
Knight::~Knight() {}

bool Knight::checkMove(Point dst, Board b)
{
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    else if (((_place.getX() + 2 == dst.getX() && _place.getY() + 1 == dst.getY()) ||
        (_place.getX() + 2 == dst.getX() && _place.getY() - 1 == dst.getY()) ||
        (_place.getX() - 2 == dst.getX() && _place.getY() + 1 == dst.getY()) ||
        (_place.getX() - 2 == dst.getX() && _place.getY() - 1 == dst.getY()) ||
        (_place.getX() - 1 == dst.getX() && _place.getY() + 2 == dst.getY()) ||
        (_place.getX() + 1 == dst.getX() && _place.getY() + 2 == dst.getY()) ||
        (_place.getX() - 1 == dst.getX() && _place.getY() - 2 == dst.getY()) ||
        (_place.getX() + 1 == dst.getX() && _place.getY() - 2 == dst.getY())) &&
        (b.getSpotInBoard(&dst) == nullptr|| b.getSpotInBoard(&dst)->getColor() != getColor()))
    {
        return true;
    }
    return false;
}