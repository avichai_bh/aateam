#include "Rook.h"

Rook::Rook(Point place, int color) : Piece(place,color,ROOK)
{}
Rook::~Rook(){}

bool Rook::checkMove(Point dst, Board b)
{
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    //checking to see its going in straight line
    if((dst.getY() == _place.getY() || dst.getX() == _place.getX()))
    {
        //checking direction
        if (dst.getY() > _place.getY())
        {
            //checking path is clear
            for (int i = _place.getY()+1; i < dst.getY(); i++)
            {
                Point* currentPoint = new Point(_place.getX(), i);
                if (b.getSpotInBoard(currentPoint) != nullptr)
                {
                    //ERROR
                    return false;
                }
                delete currentPoint;
            }
        }
        else if (dst.getY() < _place.getY())
        {
            //checking path is clear
            for (int i = _place.getY()-1; i > dst.getY(); i--)
            {
                Point* currentPoint = new Point(_place.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                delete currentPoint;
            }
        }
        //checking direction
        if (dst.getX() > _place.getX())
        {
            //checking path is clear
            for (int i = _place.getX()+1; i < dst.getX(); i++)
            {
                Point* currentPoint = new Point(i, _place.getY());
                if (b.getSpotInBoard(currentPoint) != nullptr)
                {
                    //ERROR
                    return false;
                }
                delete currentPoint;
            }
        }
        else if (dst.getX() < _place.getX())
        {
            //checking path is clear
            for (int i = _place.getX()-1; i > dst.getX(); i--)
            {
                Point* currentPoint = new Point(i, _place.getY());
                if (b.getSpotInBoard(currentPoint) != nullptr)
                {
                    //ERROR
                    return false;
                }
                delete currentPoint;
            }
        }
        return true;
    }
    return false;
}
