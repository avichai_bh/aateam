#pragma once
#include "Piece.h"


class King : public Piece
{
public:
	King();
	King(Point place, int color);
	~King();

	bool checkMove(Point dst, Board b);
	bool getCheckState();
	void setCheckState(bool state);
private:
	bool isOnCheck;
};