#pragma once

#include "Piece.h"

class Pawn : public Piece
{
public:
	Pawn();
	Pawn(Point place, int color);
	~Pawn();

	bool checkMove(Point dst, Board b);
protected:
	bool _isFirstMove;
};