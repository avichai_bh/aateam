#include "Piece.h"
#include "GameBase.h"
/*
builders and destructors for Piece
input: place, color, type
output: none
*/
Piece::Piece() {}
Piece::Piece(Point place, int color, char type) :_place(place), _color(color)
{
    if (color)
    {
        _type = type;
    }
    else
    {
        _type = std::toupper(type);
    }
}
Piece::~Piece() {}
/*
this function sets the Pieces place and iupdates the board
input: dst where to update in Board, b board to update
*/
void Piece::setPlaceOnBoard(Point dst, Board* b)
{
    b->setSpotInBoard(this, &dst);
    b->setSpotInBoard(nullptr, &_place);
    _place = dst;
}
/*
this function sets the Pieces place
input: point val to insert
*/
void Piece::setPlace(Point p)
{
    _place = p;
}
//getters
Point Piece::getPlace()
{
    return  _place;
}
//getters
int Piece::getColor()
{
    return _color;
}
char Piece::getType()
{
    return _type;
}
/*
this function checks if the move is valid and move it
input: dst destination on board and b board to set the move on
output: none(ERRORS)
*/
int Piece::move(Point dst, Board* b, GameBase* gameManager)
{
    if (this)
    {
        Point tmp_place = _place;
        if (!checkMove(dst, *b) || checkSmaeColor(_place, dst, *b) || dst.getX() > SIZE || dst.getY() > SIZE || dst.getX() < 0 || dst.getY() < 0 || gameManager->getTurn() != getColor())
        {
            if (gameManager->getTurn() != getColor())
            {
                gameManager->setTurn(!gameManager->getTurn());
                return SRC_NOT_VALID;
            }
            gameManager->setTurn(!gameManager->getTurn());
            if (!checkMove(dst, *b))
            {
                return ILLEGAL_MOVE;
            }
            else if (checkSmaeColor(_place, dst, *b))
            {
                return DST_NOT_VALID;
            }
            else if (dst.getX() > SIZE || dst.getY() > SIZE || dst.getX() < 0 || dst.getY() < 0)
            {
                return INDEX_NOT_INBOARD;
            }
            
        }

        setPlaceOnBoard(dst, b);
        bool kw = gameManager->checkCheck(gameManager->getKing(WHITE), gameManager->getKing(WHITE)->getPlace());
        bool kb = gameManager->checkCheck(gameManager->getKing(BLACK), gameManager->getKing(BLACK)->getPlace());

        if (!gameManager->checkMat(gameManager->getKing(BLACK)) && !gameManager->checkMat(gameManager->getKing(WHITE)))
        {
            int kingToCheck = 0;
            if (getColor() == WHITE)
            {
                kingToCheck = WHITE;
            }
            if (gameManager->getKing(kingToCheck)->getCheckState())
            {
                setPlaceOnBoard(tmp_place, b);
                gameManager->setTurn(!gameManager->getTurn());
                return NOT_VALID_PUTS_CHECK;
            }
        }
        else
        {
            if (_color)
            {
                std::cout << "White won the game.\n";
                return VALID_CHECKMATE;//ERROR cant move it when in check
            }
            if (!_color)
            {
                std::cout << "White won the game.\n";
                return VALID_CHECKMATE;//ERROR cant move it when in check
            }
        }
        if (getColor())
        {
            if (kb)
            {
                return VALID_MADE_CHECK;
            }
        }
        else
        {
            if (kw)
            {
                return VALID_MADE_CHECK;
            }
        }
    }
    else
    {
        gameManager->setTurn(!gameManager->getTurn());
        return SRC_NOT_VALID; //ERROR trying to move null
    }
    return VALID;
}
/*
this function checks if two pieces on the board have the same color
input: src piece 1 and dst piece 2 b the board to check on
output: bool is the same color
*/
bool Piece::checkSmaeColor(Point src, Point dst, Board b)
{
    if (b.getSpotInBoard(&dst))
    {
        if (b.getSpotInBoard(&dst)->getColor() == this->getColor())
        {
            return true;
        }
    }
    return false;
}
/*
this function check if src !+ dst
input: src, dst
output: if equal ot not
*/
bool Piece::checkSameSpot(Point src, Point dst)
{
    return src.getX() == dst.getX() && src.getY() == dst.getY();
}