#include "Point.h"
/*
this functions is a builder for Point
input: x, y
output: none
*/
Point::Point(int x, int y) : _x(x), _y(y) {}
Point::Point() {}
Point::~Point() {}
/*
this function sets the y field
input: value to insret to y
output: none
*/
void Point::setY(int y)
{
	_y = y;
}
/*
this function sets the x field
input: value to insret to x
output: none
*/
void Point::setX(int x)
{
	_x = x;
}
//getters
int Point::getX()
{
	return _x;
}
int Point::getY()
{
	return _y;
}