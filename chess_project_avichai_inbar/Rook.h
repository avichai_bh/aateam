#pragma once
#include "Piece.h"


class Rook : public Piece
{
public:
	Rook();
	Rook(Point place, int color);
	~Rook();

	bool checkMove(Point dst, Board b);
	void move(Point dst, Board b);
};
