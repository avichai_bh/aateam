#include "Board.h"
#include "Pawn.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"

Board::Board()
{
	int tmp_color = 0;
	for (int  i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++) 
		{
			Point p(j, i);
			if (i == 0 || i == SIZE - 1)
			{
				
				if (i >= SIZE - 3)
				{
					tmp_color = 1;
				}
				if (j == 0 || j == 7) 
				{
					_board[i][j] = new Rook(p, tmp_color);
				}
				else if (j == 1 || j == 6)
				{
					_board[i][j] = new Knight(p, tmp_color);
				}
				else if (j == 2 || j == 5)
				{
					_board[i][j] = new Bishop(p , tmp_color);
				}
				else if (j == 3)
				{
					_board[i][j] = new King(p, tmp_color);
				}
				else if (j == 4)
				{
					_board[i][j] = new Queen(p, tmp_color);
				}
				
			}
			else if (i == 1 || i == SIZE - 2)
			{
				if (i == 1)
				{
					_board[i][j] = new Pawn(p, 0);
				}
				else if (i == SIZE - 2)
				{
					_board[i][j] = new Pawn(p, 1);
				}
			}
			
			if (i > 1 && i < 6)
			{
				_board[i][j] = nullptr;
			}
		}
	}
}
Board::~Board(){}

/*
this function gets a point in the board
and return the Piece thaths in that spot
input: p point on Board
output Piece* piece in that loaction in the board
*/
Piece* Board::getSpotInBoard(Point* p)
{
	return _board[p->getY()][p->getX()];
}
/*
this function sets a point on the board to a piece you insert
input: newPiece new value to put in the board, p Point on the board to insert the piece
output: none
*/
void Board::setSpotInBoard(Piece* newPiece, Point* p)
{
	_board[p->getY()][p->getX()] = newPiece;	
}
/*
this function get the Boards Str
input: none
output: none
*/
string Board::getBoardStr()
{
	string boardSTR = "";

	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (_board[i][j])
			{
				boardSTR = boardSTR + _board[i][j]->getType();
				boardSTR = boardSTR + ' ';
			}
			else
			{
				boardSTR = boardSTR + '#' + ' ';
			}
		}
		boardSTR.pop_back();
		boardSTR += "\n";
	}
	boardSTR.pop_back();
	return boardSTR;
}
//getters
Piece** Board::getBoard()
{
	return &_board[0][0];
}
char* Board::getBoardStrMsg()
{
	char* boardSTR = new char(BOARD_PLUS_TURN_SIZE);
	int index = 0;
	for (int i = SIZE - 1; i >= 0; i--)
	{
		for (int j = 0; j < SIZE; j++)
		{
			if (_board[i][j])
			{
				boardSTR[index] = _board[i][j]->getType();
				index++;
			}
			else
			{
				boardSTR[index] = '#';
				index++;
			}
		}
	}
	return boardSTR;
}