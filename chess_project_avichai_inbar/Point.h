#pragma once

class Point
{
public:
	Point();
	Point(int x, int y);
	~Point();
	void setY(int y);
	void setX(int x);
	int getX();
	int getY();
private:
	int _y;
	int _x;
};