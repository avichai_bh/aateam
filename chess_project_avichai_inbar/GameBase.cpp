#include "GameBase.h"

using std::cin;
using std::cout;

GameBase::GameBase() {}
GameBase::GameBase(King* kW, King* kB, Board b, int turn) : King_W(kW), King_B(kB), _gameBoard(b), _turn(1) {}
GameBase::~GameBase() {}

/*
set kings position and color
input: color and position
output: none
*/
void GameBase::setKing(int color, Point p)
{
	if (WHITE)
	{
		King_W->setPlace(p);
	}
	else
	{
		King_B->setPlace(p);
	}
}
/*
set board
input: board to insert
output: none
*/
void GameBase::setBoard(Board b)
{
	_gameBoard = b;
}
/*
set turn
input: turn
output: none
*/
void GameBase::setTurn(int t)
{
	_turn = t;
}
//getters
King* GameBase::getKing(int color)
{
	if (color)
	{
		return King_W;
	}
	else
	{
		return King_B;
	}
}
Board* GameBase::getGameBoard()
{
	return &_gameBoard;
}
int GameBase::getTurn()
{
	return _turn;
}
/*
this function checks if theres a check on the king
input: king and king positoin
output: if on check or not
*/
bool GameBase::checkCheck(King* king, Point kingPos)
{
    for (int i = 0; i < SIZE; i++)
    {
        for (int j = 0; j < SIZE; j++)
        {
            if (_gameBoard.getBoard()[i * SIZE + j] != nullptr)
            {
                if (_gameBoard.getBoard()[i * SIZE + j]->getColor() != king->getColor())
                {
                    if (_gameBoard.getBoard()[i * SIZE + j]->checkMove(kingPos, _gameBoard) == true)
                    {
                        king->setCheckState(true);
                        return true;
                    }
                }
            }
        }
    }
    king->setCheckState(false);
    return false;
}
/*
this function checks mat on the king 
input: king to check
output: if in mat or not
*/
bool GameBase::checkMat(King* king)
{
    bool tmpCheckState = king->getCheckState();
    Point k_place = king->getPlace();
    Piece** g_board = _gameBoard.getBoard();
    bool mat = true;
    if (checkCheck(king, k_place))
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (0 <= (k_place.getX() + j) && (k_place.getX() + j) < SIZE && 0 <= (k_place.getY() + i) && (k_place.getY() + i) < SIZE)
                {
                    if ((!g_board[(k_place.getY() + i) * SIZE + (k_place.getX() + j)]) ||
                        (g_board[((k_place.getY() + i) * SIZE) + (k_place.getX() + j)]->getColor() != king->getColor()))
                    {
                        if (k_place.getY() + i < SIZE && k_place.getY() + i >= 0 && k_place.getX() + j < SIZE && k_place.getX() + j >= 0)
                        {
                            if (!checkCheck(king, Point(k_place.getX() + j, k_place.getY() + i)))
                            {
                                king->setCheckState(tmpCheckState);
                                mat = false;
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        mat = false;
    }

    king->setCheckState(tmpCheckState);
    return mat;
}
/*
this function gets the point from graphics and transforms it to an array index of the board
input: msg from graphics
output: Point on board
*/
Point GameBase::getPointFromGraphics(string point)
{
    char pointX = char(point[0] - 49);
    char pointY = point[1];
   
    return Point(pointX-TO_INT, SIZE - (pointY-TO_INT));
}
/*
this function moves a piece and changes a turn and returns a massag to the graphics
input: src, dst
output: msg to graphics
*/
int GameBase::round(Point* src, Point* dst)
{
    int move_r = 0;
    string board = this->getGameBoard()->getBoardStr();
    move_r = _gameBoard.getSpotInBoard(src)->move(*dst, &_gameBoard, this);
	std::cout << _gameBoard.getBoardStr() << std::endl;
    std::cout << "\n\n\n";
    if (_turn == WHITE)
    {
        _turn = BLACK;
    }
    else if (_turn == BLACK)
    {
        _turn = WHITE;
    }
    return move_r;
}
