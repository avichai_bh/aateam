#pragma once
#include "Piece.h"

class Board;
class Piece;
class Bishop : public Piece
{
public:
	Bishop();
	Bishop(Point place, int color);
	~Bishop();

	bool checkMove(Point dst, Board b);
};
