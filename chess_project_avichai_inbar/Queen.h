#pragma once
#include "Piece.h"

class Queen : public Piece
{
public:
	Queen();
	Queen(Point place, int color);
	~Queen();

	bool checkMove(Point dst, Board b);
	bool checkDiagnale(Point dst, Board b);
	bool checkLine(Point dst, Board b);
};
