#pragma once
#include "Board.h"
#include "King.h"

#define TO_INT '0'


class GameBase
{
public:
	GameBase();
	GameBase(King* kW, King* kB, Board b, int turn);
	~GameBase();

	void setKing(int color, Point p);
	void setBoard(Board b);
	void setTurn(int t);

	King* getKing(int color);
	Board* getGameBoard();
	int getTurn();
	
	Point getPointFromGraphics(string point);
	bool checkCheck(King* king, Point kinhPos);
	bool checkMat(King* king);
	int round(Point* src, Point* dst);
private:
	King* King_W;
	King* King_B;
	Board _gameBoard;
	int _turn; // 1- white down \ 0 - black up
	
};