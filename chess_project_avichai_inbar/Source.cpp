/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project,
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "Pawn.h"
#include "GameBase.h"
#include "King.h"
#include "Queen.h"
#include "Rook.h"
#include "Bishop.h"
#include "Knight.h"
#include "Piece.h"
#define SIZE 8
#define WHITE 1
#define BLACK 0
using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));


	Pipe p;
	bool isConnect = p.connect();
	// 1- white down \ 0 - black up
	Board* game_board = new Board();
	Point* kp = new Point(3, 0);//first king place
	King* k2 = (King*)(game_board->getSpotInBoard(kp));
	kp->setY(SIZE - 1);
	kp->setX(3);//second king place
	King* k1 = (King*)(game_board->getSpotInBoard(kp));

	GameBase* game_manager = new GameBase(k1, k2, *game_board, WHITE);
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else
		{
			p.close();
			return;
		}
	}


	char msgToGraphics[1024];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"); // just example...

	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4           (move e2 to e4)

		// YOUR CODE
		string srcStr = "";
		srcStr = srcStr + msgFromGraphics[0] + msgFromGraphics[1];
		string dstStr = "";
		dstStr = dstStr + msgFromGraphics[2] + msgFromGraphics[3];

		Point src = game_manager->getPointFromGraphics(srcStr);
		Point dst = game_manager->getPointFromGraphics(dstStr);

		char move_type = game_manager->round(&src, &dst) + '0';
		char msg[1024]={};
		msg[0] = move_type;
		/*
		int r = rand() % 10; // just for debugging......
		msgToGraphics[0] = (char)(1 + '0');
		msgToGraphics[1] = 0;
		*/

		strcpy_s(msgToGraphics, msg); // msgToGraphics should contain the result of the operation

		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
}