#include "Pawn.h"

Pawn::Pawn(){}
Pawn::Pawn(Point place, int color): Piece(place, color, PAWN){}
Pawn::~Pawn(){}

bool Pawn::checkMove(Point dst, Board b)
{  
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    else if (_isFirstMove)// option to do two
    {
        if (_color)//white
        {
            if ((_place.getY() - dst.getY() == 1 || _place.getY() - dst.getY() == 2) && dst.getX() == _place.getX()&&b.getSpotInBoard(&dst)==nullptr)
            {
                if (_place.getY() - dst.getY() == 2)//chacks to see if we jumped over a piece
                {
                    Point p = Point(dst.getX(), dst.getY() +1);
                    if (b.getSpotInBoard(&p) != nullptr)
                    {
                        return false;
                    }
                }
                _isFirstMove = false;
                return true;
            }
            if (_place.getY() - dst.getY() == 1 && (dst.getX() == _place.getX() + 1 || dst.getX() == _place.getX() - 1) && b.getSpotInBoard(&dst) != nullptr)
            {
                _isFirstMove = false;
                return true;
            }
        }
        else//black
        {
            if ((dst.getY() - _place.getY() == 1 || dst.getY() - _place.getY() == 2) && dst.getX() == _place.getX()&& b.getSpotInBoard(&dst) == nullptr)
            {
                if (dst.getY() - _place.getY() == 2)//chacks to see if we jumped over a piece
                {
                    Point p = Point(dst.getX(), dst.getY() - 1);
                    if (b.getSpotInBoard(&p) != nullptr)
                    {
                        return false;
                    }
                }
                _isFirstMove = false;
                return true;
            }
            if (dst.getY() - _place.getY() == 1 && (dst.getX() == _place.getX() + 1 || dst.getX() == _place.getX() - 1) && b.getSpotInBoard(&dst) != nullptr)
            {
                _isFirstMove = false;
                return true;
            }
        }
    }
    else
    {
        if (_color)//white
        {
            
            if (_place.getY() - dst.getY() == 1 && dst.getX() == _place.getX() && b.getSpotInBoard(&dst) == nullptr)
            {
                return true;
            }
            if (_place.getY() - dst.getY() == 1 && (dst.getX() == _place.getX() + 1 || dst.getX() == _place.getX() - 1) && b.getSpotInBoard(&dst) != nullptr)
            {
                _isFirstMove = false;
                return true;
            }
        }
        else//black
        {
            if (dst.getY() - _place.getY() == 1 && dst.getX() == _place.getX() && b.getSpotInBoard(&dst) == nullptr)
            {
                return true;
            }
            if (dst.getY() - _place.getY() == 1 && (dst.getX() == _place.getX() + 1 || dst.getX() == _place.getX() - 1) && b.getSpotInBoard(&dst) != nullptr)
            {
                _isFirstMove = false;
                return true;
            }
        }
    }
    return false;
}