#include "King.h"
/*
this funcrtions are builders and constructors for king
input: place, color
output: none
*/
King::King(){}
King::King(Point place, int color): Piece(place, color, KING)
{
    isOnCheck = false;
}
King::~King(){}

bool King::checkMove(Point dst, Board b)
{
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    else if ((_place.getX() + 1 == dst.getX() && _place.getY() == dst.getY()) ||
        (_place.getX() + 1 == dst.getX() && _place.getY() == dst.getY()) ||
        (_place.getX() == dst.getX() && _place.getY() - 1 == dst.getY()) ||
        (_place.getX() == dst.getX() && _place.getY() + 1 == dst.getY()) ||
        (_place.getX() + 1 == dst.getX() && _place.getY() + 1 == dst.getY()) ||
        (_place.getX() + 1 == dst.getX() && _place.getY() - 1 == dst.getY()) ||
        (_place.getX() - 1 == dst.getX() && _place.getY() - 1 == dst.getY()) ||
        (_place.getX() - 1 == dst.getX() && _place.getY() + 1 == dst.getY()))
    {
        return true;
    }
    return false;
}
/*
this function set the kings check state
input: value to insert
output: none
*/
void King::setCheckState(bool state)
{
    isOnCheck = state;
}
//getters
bool King::getCheckState()
{
    return isOnCheck;
}