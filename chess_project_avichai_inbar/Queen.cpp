#include "Queen.h"

Queen::Queen() {}
Queen::Queen(Point place, int color):Piece(place, color, QUEEN){}
Queen::~Queen(){}

bool Queen::checkMove(Point dst, Board b)
{
    if (checkSameSpot(getPlace(), dst))
    {
        return false;
    }
    if (dst.getX() == _place.getX() || dst.getY() == _place.getY())
    {
        return checkLine(dst, b);
    }
    else
    {
        return checkDiagnale(dst, b);
    } 
}
/*
this function check the diagnales in the board from current queens position to dst
input: dst, board to check
output: if can move or not
*/
bool Queen::checkDiagnale(Point dst, Board b)
{
    if (abs(dst.getY() - _place.getY()) == abs(dst.getX() - _place.getX()))
    {
        Point src(_place.getX(), _place.getY());//tmp holder of piece spot
        //checking direction
        if (dst.getY() > _place.getY())
        {
            if (dst.getX() > _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() + 1; i < dst.getY(); i++)
                {
                    src.setX(src.getX() + 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            else if (dst.getX() < _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() + 1; i < dst.getY(); i++)
                {
                    src.setX(src.getX() - 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;

                }
            }
        }

        else if (dst.getY() < _place.getY())
        {
            if (dst.getX() > _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() - 1; i > dst.getY(); i--)
                {                    
                    src.setX(src.getX() + 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;                 
                }
            }
            else if (dst.getX() < _place.getX())
            {
                //checking path is clear
                for (int i = _place.getY() - 1; i > dst.getY(); i--)
                {
                    src.setX(src.getX() - 1);
                    Point* currentPoint = new Point(src.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
        }
        return true;
    }
    return false;
}
/*
this function check the line in the board from current queens position to dst
input: dst, board to check
output: if can move or not
*/
bool Queen::checkLine(Point dst, Board b)
{
    if ((dst.getY() == _place.getY() || dst.getX() == _place.getX()))
    {
        //checking to see its going in straight line
        if (dst.getY() == _place.getY() || dst.getX() == _place.getX())
        {
            //checking direction
            if (dst.getY() > _place.getY())
            {
                //checking path is clear
                for (int i = _place.getY()+1; i < dst.getY(); i++)
                {
                    Point* currentPoint = new Point(_place.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            else if (dst.getY() < _place.getY())
            {
                //checking path is clear
                for (int i = _place.getY()-1; i > dst.getY(); i--)
                {
                    Point* currentPoint = new Point(_place.getX(), i);
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            //checking direction
            if (dst.getX() > _place.getX())
            {
                //checking path is clear
                for (int i = _place.getX()+1; i < dst.getX(); i++)
                {
                    Point* currentPoint = new Point(i, _place.getY());
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
            else if (dst.getX() < _place.getX())
            {
                //checking path is clear
                for (int i = _place.getX()-1; i > dst.getX(); i--)
                {
                    Point* currentPoint = new Point(i, _place.getY());
                    if (b.getSpotInBoard(currentPoint) != nullptr)
                    {
                        //ERROR
                        return false;
                    }
                    delete currentPoint;
                }
            }
        }
        return true;
    }
    return false;
}