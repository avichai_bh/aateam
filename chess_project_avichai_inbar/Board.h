#pragma once
#include "Piece.h"
#include <iostream>
#include "string.h"

#define BOARD_PLUS_TURN_SIZE 8*8+1
#define SIZE 8

using std::string;
class Piece;

class Board
{
public:
	Board();
	~Board();

	Piece* getSpotInBoard(Point* p);
	void setSpotInBoard(Piece* newPiece, Point* p);

	Piece** getBoard();
	string getBoardStr();
	char* getBoardStrMsg();

protected:
	Piece* _board[SIZE][SIZE];
};