#pragma once
#include "Point.h"
#include "Board.h"
#define VALID 0
#define VALID_MADE_CHECK 1
#define SRC_NOT_VALID 2
#define DST_NOT_VALID 3
#define NOT_VALID_PUTS_CHECK 4
#define INDEX_NOT_INBOARD 5
#define ILLEGAL_MOVE 6
#define NUT_VALID_SAME_POINT 7
#define VALID_CHECKMATE 8

#define SIZE 8
#define WHITE 1
#define BLACK 0

#define BLANK '#'
#define QUEEN 'q'
#define KING 'k'
#define PAWN 'p'
#define BISHOP 'b'
#define ROOK 'r'
#define KNIGHT 'n'
class Board;
class GameBase;
class Pawn;
class Queen;
class Bishop;
class Knight;
class King;
class Rook;

class Piece
{
public:
	Piece();
	Piece(Point place, int color, char type);
	~Piece();
;
	/*
	this function checks if the move is legal
	input: dst, board the check path
	output: bool if is legal ir not
	*/
	virtual bool checkMove(Point dst, Board b) = 0;

	int move(Point dst, Board* b, GameBase* gameManager);
	void setPlaceOnBoard(Point dst, Board* b);
	
	void setPlace(Point p);

	Point getPlace();
	int getColor();
	char getType();

	bool checkSmaeColor(Point src, Point dst, Board b);
	bool checkSameSpot(Point src, Point dst);

protected:
	Point _place;
	int _color; // 1- white down \ 0 - black up
	char _type;
};