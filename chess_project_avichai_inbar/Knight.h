#pragma once
#include "Piece.h"

class Knight : public Piece
{
public:
	Knight();
	Knight(Point place, int color);
	~Knight();

	bool checkMove(Point dst, Board b);

private:

};
